package ru.kuzin.tm.unit.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.kuzin.tm.config.ApplicationConfiguration;
import ru.kuzin.tm.config.WebApplicationConfiguration;
import ru.kuzin.tm.dto.TaskDTO;
import ru.kuzin.tm.marker.UnitCategory;
import ru.kuzin.tm.util.UserUtil;

import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebApplicationConfiguration.class, ApplicationConfiguration.class})
@Category(UnitCategory.class)
public class TaskEndpointTest {

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @NotNull
    @Autowired
    private WebApplicationContext wac;

    @NotNull
    private static final String PROJECT_URL = "http://localhost:8080/api/task";

    @NotNull
    private static final String PROJECTS_URL = "http://localhost:8080/api/tasks";

    @NotNull
    private final TaskDTO taskFirst = new TaskDTO("Task Test 1");

    @NotNull
    private final TaskDTO taskSecond = new TaskDTO("Task Test 2");

    @NotNull
    private final TaskDTO taskThird = new TaskDTO("Task Test 3");

    @NotNull
    private final TaskDTO taskFourth = new TaskDTO("Task Test 4");

    @Before
    public void initTest() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("admin", "admin");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        taskFirst.setUserId(UserUtil.getUserId());
        taskSecond.setUserId(UserUtil.getUserId());
        taskThird.setUserId(UserUtil.getUserId());
        taskFourth.setUserId(UserUtil.getUserId());
        taskPost(taskFirst);
        taskPost(taskSecond);
    }

    @After
    public void clear() {
        tasksDelete(tasksGet());
    }

    @SneakyThrows
    private TaskDTO taskGet(@NotNull final String id) {
        @NotNull final String url = PROJECT_URL + "/" + id;
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        if ("".equals(json)) return null;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, TaskDTO.class);
    }

    @SneakyThrows
    private List<TaskDTO> tasksGet() {
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(PROJECTS_URL)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        if ("".equals(json)) return null;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return Arrays.asList(objectMapper.readValue(json, TaskDTO[].class));
    }


    @Test
    @SneakyThrows
    public void taskGetTest() {
        Assert.assertEquals(taskFirst.getName(), taskGet(taskFirst.getId()).getName());
    }

    @Test
    @SneakyThrows
    public void tasksGetTest() {
        Assert.assertEquals(2, tasksGet().size());
        Assert.assertNotNull(
                tasksGet().stream()
                        .filter(m -> taskFirst.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
        Assert.assertNotNull(
                tasksGet().stream()
                        .filter(m -> taskSecond.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
    }

    @SneakyThrows
    private void taskPost(@NotNull final TaskDTO task) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(task);
        mockMvc.perform(MockMvcRequestBuilders.post(PROJECT_URL).content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @SneakyThrows
    private void tasksPost(@NotNull final List<TaskDTO> tasks) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(tasks);
        mockMvc.perform(MockMvcRequestBuilders.post(PROJECTS_URL).content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    public void taskPostTest() {
        Assert.assertEquals(2, tasksGet().size());
        Assert.assertNull(
                tasksGet().stream()
                        .filter(m -> taskThird.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
        taskPost(taskThird);
        Assert.assertEquals(3, tasksGet().size());
        Assert.assertNotNull(
                tasksGet().stream()
                        .filter(m -> taskThird.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
    }

    @Test
    @SneakyThrows
    public void tasksPostTest() {
        Assert.assertEquals(2, tasksGet().size());
        tasksPost(Arrays.asList(taskThird, taskFourth));
        Assert.assertEquals(4, tasksGet().size());
        Assert.assertNotNull(
                tasksGet().stream()
                        .filter(m -> taskThird.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
        Assert.assertNotNull(
                tasksGet().stream()
                        .filter(m -> taskFourth.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
    }

    @SneakyThrows
    private void taskPut(@NotNull final TaskDTO task) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(task);
        mockMvc.perform(MockMvcRequestBuilders.post(PROJECT_URL).content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @SneakyThrows
    private void tasksPut(@NotNull final List<TaskDTO> tasks) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(tasks);
        mockMvc.perform(MockMvcRequestBuilders.post(PROJECTS_URL).content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    public void taskPutTest() {
        Assert.assertNotEquals("Name for task 1", taskGet(taskFirst.getId()).getName());
        Assert.assertNull(
                tasksGet().stream()
                        .filter(m -> taskThird.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
        taskFirst.setName("Name for task 1");
        taskPut(taskFirst);
        Assert.assertEquals("Name for task 1", taskGet(taskFirst.getId()).getName());
    }

    @Test
    @SneakyThrows
    public void tasksPutTest() {
        Assert.assertNotEquals("Name for task 1", taskGet(taskFirst.getId()).getName());
        Assert.assertNotEquals("Name for task 2", taskGet(taskSecond.getId()).getName());
        taskFirst.setName("Name for task 1");
        taskSecond.setName("Name for task 2");
        tasksPost(Arrays.asList(taskFirst, taskSecond));
        Assert.assertEquals("Name for task 1", taskGet(taskFirst.getId()).getName());
        Assert.assertEquals("Name for task 2", taskGet(taskSecond.getId()).getName());
    }

    @SneakyThrows
    private void taskDelete(@NotNull final String id) {
        @NotNull final String url = PROJECT_URL + "/" + id;
        mockMvc.perform(MockMvcRequestBuilders.delete(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @SneakyThrows
    private void tasksDelete(@NotNull final List<TaskDTO> tasks) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(tasks);
        mockMvc.perform(MockMvcRequestBuilders.delete(PROJECTS_URL).content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    public void taskDeleteTest() {
        Assert.assertEquals(2, tasksGet().size());
        Assert.assertNotNull(
                tasksGet().stream()
                        .filter(m -> taskFirst.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
        taskDelete(taskFirst.getId());
        Assert.assertEquals(1, tasksGet().size());
        Assert.assertNull(
                tasksGet().stream()
                        .filter(m -> taskFirst.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
    }

    @Test
    @SneakyThrows
    public void tasksDeleteTest() {
        Assert.assertEquals(2, tasksGet().size());
        tasksDelete(Arrays.asList(taskFirst, taskSecond));
        Assert.assertEquals(0, tasksGet().size());
    }

}