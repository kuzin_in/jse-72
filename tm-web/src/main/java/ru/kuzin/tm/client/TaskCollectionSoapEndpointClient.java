package ru.kuzin.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.kuzin.tm.api.endpoint.ITaskCollectionEndpoint;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import java.net.URL;

public class TaskCollectionSoapEndpointClient {

    @SneakyThrows
    public static ITaskCollectionEndpoint getInstance(@NotNull final String baseURL) {
        @NotNull final String wsdl = baseURL + "/jaxws/TaskCollectionEndpointWS?wsdl";
        @NotNull final URL url = new URL(wsdl);
        @NotNull final String lp = "TaskCollectionEndpointImplService";
        @NotNull final String ns = "http://endpoint.tm.kuzin.ru/";
        @NotNull final QName name = new QName(ns, lp);
        @NotNull final ITaskCollectionEndpoint result = Service.create(url, name).getPort(ITaskCollectionEndpoint.class);
        @NotNull final BindingProvider bindingProvider = (BindingProvider) result;
        bindingProvider.getRequestContext().put(BindingProvider.SESSION_MAINTAIN_PROPERTY, true);
        return result;
    }

}