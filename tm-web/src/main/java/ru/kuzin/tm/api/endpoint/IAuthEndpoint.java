package ru.kuzin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.kuzin.tm.dto.CredentialsDTO;
import ru.kuzin.tm.dto.Result;
import ru.kuzin.tm.dto.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IAuthEndpoint {

    @NotNull
    @PostMapping("/login")
    @WebMethod
    Result login(
            @WebParam(name = "credentials", partName = "credentials")
            @NotNull @RequestBody CredentialsDTO credentials);

    @Nullable
    @GetMapping
    @WebMethod
    UserDTO profile();

    @NotNull
    @PostMapping("/logout")
    @WebMethod
    Result logout();

}