package ru.kuzin.tm.listener.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.kuzin.tm.dto.model.ProjectDTO;
import ru.kuzin.tm.dto.request.ProjectListRequest;
import ru.kuzin.tm.dto.response.ProjectListResponse;
import ru.kuzin.tm.enumerated.EntitySort;
import ru.kuzin.tm.event.ConsoleEvent;
import ru.kuzin.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Component
public final class ProjectListListener extends AbstractProjectListener {

    @NotNull
    private static final String NAME = "project-list";

    @NotNull
    private static final String DESCRIPTION = "Show project list.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@projectListListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[SHOW PROJECTS]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(EntitySort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final EntitySort sort = EntitySort.toSort(sortType);
        @NotNull final ProjectListRequest request = new ProjectListRequest(getToken());
        @Nullable final ProjectListResponse response = projectEndpoint.listProject(request);
        if (response.getProjects() == null) response.setProjects(Collections.emptyList());
        @Nullable final List<ProjectDTO> projects = projectEndpoint.listProject(request).getProjects();
        renderProjects(projects);
    }

    private void renderProjects(@NotNull final List<ProjectDTO> projects) {
        int index = 1;
        for (@Nullable final ProjectDTO project : projects) {
            if (project == null) continue;
            System.out.println(index + ". " + project.getName());
            index++;
        }
    }

}